package com.example.logapplication.workers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.joda.time.DateTime;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class LogWorkerDaily extends Worker {

    public LogWorkerDaily(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {
        Date currentTime = Calendar.getInstance().getTime();
        appendLogInFile(currentTime.toString());

        return Result.success();
    }

    public void appendLogInFile(String log) {
        //File logFile = new File( Environment.getExternalStorageDirectory().toString()+"/log_daily.txt");
        //for emulator
        File logFile = new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/log_daily.txt");

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
                Log.d("LogApp", "File created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(log);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("LogApp", "Daily log created at " + DateTime.now().toString());
    }
}
