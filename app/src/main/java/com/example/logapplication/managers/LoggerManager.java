package com.example.logapplication.managers;

import android.content.Context;

import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;


import com.example.logapplication.workers.LogWorkerDaily;
import com.example.logapplication.workers.LogWorkerHourly;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.concurrent.TimeUnit;

public class LoggerManager {

    //The hour wanted for the daily log to be logged
    private static final int SELF_REMINDER_HOUR = 1;
    private final SharedPreferencesManager mSharedPreferencesManager;
    private final Context mContext;


    public LoggerManager(Context context, SharedPreferencesManager sharedPreferencesManager) {
        mSharedPreferencesManager = sharedPreferencesManager;
        mContext = context;
    }

    public void setupLogWorkerHourly() {
        //Launch for first time after boot
        if (mSharedPreferencesManager.isFirstLaunchAfterBoot()) {
            mSharedPreferencesManager.setIsFirstLaunchAfterBoot(false);
            WorkManager.getInstance(mContext).cancelAllWork();
            PeriodicWorkRequest workRequestHourly =
                    new PeriodicWorkRequest.Builder(
                            LogWorkerHourly.class,
                            60 ,
                            TimeUnit.MINUTES)
                            .addTag("hourlyWorkRequest")
                            .build();

            WorkManager.getInstance(mContext).enqueueUniquePeriodicWork("hourlyWorkRequest",
                    ExistingPeriodicWorkPolicy.REPLACE, workRequestHourly);
        }
    }

    public void setupLogWorkerDaily() {
        long delay;
        if (DateTime.now().getHourOfDay() < SELF_REMINDER_HOUR) {
            delay = new Duration(DateTime.now(), DateTime.now().withTimeAtStartOfDay().plusHours(SELF_REMINDER_HOUR)).getStandardMinutes();
        } else {
            delay = new Duration(DateTime.now(), DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_REMINDER_HOUR)).getStandardMinutes();
        }


        PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(
                LogWorkerDaily.class,
                24,
                TimeUnit.HOURS)
                .setInitialDelay(delay, TimeUnit.MINUTES)
                .addTag("dailyWorkRequest")
                .build();


        WorkManager.getInstance(mContext)
                .enqueueUniquePeriodicWork("dailyWorkRequest", ExistingPeriodicWorkPolicy.REPLACE, workRequest);
    }

    public void startLogging() {
        setupLogWorkerHourly();
        setupLogWorkerDaily();
    }

}
