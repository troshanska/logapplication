package com.example.logapplication.managers;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {

    private static final String MY_PREFERENCES = "mypref";

    private static final String KEY_FIRST_BOOT = "firstBoot";

    private SharedPreferences msharedPreferences;

    /**
     * @param context is needed to access shared prefs
     */
    public SharedPreferencesManager(Context context) {
        this.msharedPreferences = context.getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
    }

    public boolean isFirstLaunchAfterBoot() {
        return msharedPreferences.getBoolean(KEY_FIRST_BOOT, true);
    }

    public void setIsFirstLaunchAfterBoot(boolean isFirstLaunchAfterBoot) {
        msharedPreferences.edit().putBoolean(KEY_FIRST_BOOT, isFirstLaunchAfterBoot).apply();
    }


}
