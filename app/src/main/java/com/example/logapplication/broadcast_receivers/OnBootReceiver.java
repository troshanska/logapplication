package com.example.logapplication.broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.logapplication.managers.SharedPreferencesManager;

import org.joda.time.DateTime;

public class OnBootReceiver extends BroadcastReceiver {

    private final SharedPreferencesManager mSharedPreferencesManager;

    public OnBootReceiver(SharedPreferencesManager sharedPreferencesManager) {

        mSharedPreferencesManager = sharedPreferencesManager;

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            mSharedPreferencesManager.setIsFirstLaunchAfterBoot(true);
            Toast.makeText(context, "Boot completed!", Toast.LENGTH_SHORT).show();
            Log.d("LogApp", "Boot log created at " + DateTime.now().toString());
        }
    }
}
