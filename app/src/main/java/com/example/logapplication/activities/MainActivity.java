package com.example.logapplication.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.logapplication.R;
import com.example.logapplication.managers.LoggerManager;
import com.example.logapplication.managers.SharedPreferencesManager;


public class MainActivity extends AppCompatActivity {

    private static final int STORAGE_PERMISSION_REQUEST_CODE = 777;

    private SharedPreferencesManager mSharedPreferencesManager;
    private LoggerManager mLoggerManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        checkForPermission();


    }

    private void initData() {
        mSharedPreferencesManager = new SharedPreferencesManager(this);
        mLoggerManager = new LoggerManager(this, mSharedPreferencesManager);
    }


    private void checkForPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                //Permission is granted
                mLoggerManager.startLogging();
            } else {
                //Ask for permission
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST_CODE);
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            mLoggerManager.startLogging();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLoggerManager.startLogging();
            } else {
                Toast.makeText(this, "You can't use this app without allowing to write into storage.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}

